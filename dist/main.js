(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

class grades_chool {

    constructor() {
        this.grades_ = {}
        console.log("New school created");
    }

    roster() {
        return JSON.parse(JSON.stringify(this.grades_));
    }

    add(student, index) {
        if (this.grades_[index]) {
            this.grades_[index].push(student)
            this.grades_[index] = this.grades_[index].sort()
        }
        else
            this.grades_[index] = [student];
    }

    grade(index) {
        if (this.grades_[index])
            return JSON.parse(JSON.stringify(this.grades_[index].sort()))
        else
            return []
    }
};

module.exports = grades_chool;
},{}],2:[function(require,module,exports){
var GradeSchool = require("./grade-school_module")

$(document).ready(function () {
    var school = new GradeSchool();
    $('#roster_button').click(function () {
        console.log(school.roster());
    })
    $('#add_student').click(function () {
        school.add($("#add_student_input").val(), $("#add_student_grade").val())
        console.log(school.roster());
    })
})
},{"./grade-school_module":1}]},{},[2]);
