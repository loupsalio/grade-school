# Grade-School

[Grade-School](https://grade-school.herokuapp.com/) is a web javascript project.
Creating a local grade listing service with a web interface.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

```bash
sudo apt install git #Git versioning system
sudo apt install nodejs #NodeJS / npm
```
*Tested Environments :*
* Debian 8
* Debian 9
* Docker Node Image (v11.11)

### Installing

First check you installed the requisites, then clone the repository and install the dependencies :

```bash
git clone https://gitlab.com/loupsalio/grade-school
cd grade-school
npm i --quiet
npm i mocha -g
npm i browserify -g
``` 

### Running the tests

All tests are in the tests/index.js file

If you want to add some tests, just check the basic fonctions as exemple

```bash
npm test
```

### Running the project on local system

You can access to the project directly from the index.html file.

Or you can run the Express server (linked) :

```bash
npm start
```

The access the project on the port 3000 of [localhost](http://localhost:3000)

## Edit the project

If you want to edit the project, you can use the main.js file in the root folder of the project. As the project structur use NodeJS fonctions, you need to execute *browserify* to apply your modifications.

```bash
npm run reload
# or
browserify main.js > dist/main.js
```

You do not need to execute *browserify* to run tests.

Do not forget to clear your Javascript cache of your web browser is you reload the web page.
*On firefox : **Ctrl - maj - R***

## Versioning system

The project is versioned with git system.

Structure of the project versioning : 
* Master (Production branch)
* develop (Pre-production branch)
* * functionality_1 (on_going_branch)
* * functionality_2 
* * ...

To add a functionality, you need to create a new branch.
When you implemented the functionality, you push on your branch and create a merge request to the **develop** branch.

When a branch is merged to develop, it is deleted, so be sure to complete the functionality requirements before creating a merge request.

### Testing system

Each time you push, the file test/index.js will be run in the gitlab runner on the node:latest Docker image.

### Deployment system

On the develop and master branch, when there is a push, if the tests are passed, the project will be uploaded online on the pre-production server for the develop branch :
* https://grade-school-pre-prod.herokuapp.com/

On the production server for master branch: 
* https://grade-school.herokuapp.com/

## Authors

* **Lucas CLEMENCEAU** - *Developer* - [Loupsalio](https://gitlab.com/loupsalio)
