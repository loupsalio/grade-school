'use strict';

class grades_chool {

    constructor() {
        this.grades_ = {}
        console.log("New school created");
    }

    roster() {
        return JSON.parse(JSON.stringify(this.grades_));
    }

    add(student, index) {
        if (this.grades_[index]) {
            this.grades_[index].push(student)
            this.grades_[index] = this.grades_[index].sort()
        }
        else
            this.grades_[index] = [student];
    }

    grade(index) {
        if (this.grades_[index])
            return JSON.parse(JSON.stringify(this.grades_[index].sort()))
        else
            return []
    }
};

module.exports = grades_chool;