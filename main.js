var GradeSchool = require("./grade-school_module")

$(document).ready(function () {
    var school = new GradeSchool();
    $('#roster_button').click(function () {
        console.log(school.roster());
    })

    $('#add_student').click(function () {
        school.add($("#add_student_input").val(), $("#add_student_grade").val())
        console.log(school.roster());
    })
})